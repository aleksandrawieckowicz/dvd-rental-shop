package sql;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Wypozyczenie {

	private int idWypozyczenie;
	private int idKlient;
	private int idEgzemplarz;
	private long dataWypozyczenia;
	private Long dataZwrotu;
	private double kara;
	private String status;
	

	public Wypozyczenie( int idKlient, int idEgzemplarz, long dataWypozyczenia, Long dataZwrotu, double kara, String status) {
		
		super();
		this.idKlient = idKlient;
		this.idEgzemplarz = idEgzemplarz;
		this.dataWypozyczenia = dataWypozyczenia;
		this.dataZwrotu = dataZwrotu;
		this.kara = kara;
		this.status = status;
	}
	
	

	public Wypozyczenie() {
		super();
	}



	public int getIdWypozyczenie() {
		return idWypozyczenie;
	}

	public void setIdWypozyczenie(int idWypozyczenie) {
		this.idWypozyczenie = idWypozyczenie;
	}

	public int getIdKlient() {
		return idKlient;
	}

	public void setIdKlient(int idKlient) {
		this.idKlient = idKlient;
	}

	public int getIdEgzemplarz() {
		return idEgzemplarz;
	}

	public void setIdEgzemplarz(int idEgzemplarz) {
		this.idEgzemplarz = idEgzemplarz;
	}

	public double getKara() {
		return kara;
	}

	public void setKara(double kara) {
		this.kara = kara;
	}

	public long getDataWypozyczenia() {
		return dataWypozyczenia;
	}

	public void setDataWypozyczenia(long dataWypozyczenia) {
		this.dataWypozyczenia = dataWypozyczenia;
	}

	public Long getDataZwrotu() {
		return dataZwrotu;
	}

	public void setDataZwrotu(Long dataZwrotu) {
		this.dataZwrotu = dataZwrotu;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String makeDateFormat(Long dateInLong) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(new Date(dateInLong));
	}
	
}
