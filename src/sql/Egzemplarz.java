package sql;

public class Egzemplarz {
	private int idEgzemplarz;
	private int idPlyta;
	
	public Egzemplarz(int idEgzemplarz, int idPlyta) {
		super();
		this.idEgzemplarz = idEgzemplarz;
		this.idPlyta = idPlyta;
	}
	
	public Egzemplarz() {
		super();
	}
	
	public int getIdEgzemplarz() {
		return idEgzemplarz;
	}
	public void setIdEgzemplarz(int idEgzemplarz) {
		this.idEgzemplarz = idEgzemplarz;
	}
	public int getIdPlyta() {
		return idPlyta;
	}
	public void setIdPlyta(int idPlyta) {
		this.idPlyta = idPlyta;
	}
	

}
