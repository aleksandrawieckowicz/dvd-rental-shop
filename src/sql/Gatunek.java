package sql;

public class Gatunek {

	
	private int idGatunek;
	private String nazwaGatunek;
	
	public Gatunek(String nazwaGatunek) {
		super();
		this.nazwaGatunek = nazwaGatunek;
	}
	
	public Gatunek() {
		super();
	}
	
	public int getIdGatunek() {
		return idGatunek;
	}
	public void setIdGatunek(int idGatunek) {
		this.idGatunek = idGatunek;
	}
	public String getNazwaGatunek() {
		return nazwaGatunek;
	}
	public void setNazwaGatunek(String nazwaGatunek) {
		this.nazwaGatunek = nazwaGatunek;
	}

	@Override
	public String toString() {
		return nazwaGatunek;
	}

	
	
	
}
