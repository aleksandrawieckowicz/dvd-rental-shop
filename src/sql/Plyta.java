package sql;

public class Plyta {
	private int idPlyta;
	private String tytul;
	private Rezyser rezyser;
	private Gatunek gatunek;
	private double cena;
	
	//private Rezyser rezyser; utworzyc to pole zamiast id autor i odwo�ywac si� do ca�ego obiektu
	
	public Plyta(int idPlyta, String tytul, Rezyser rezyser, Gatunek gatunek, double cena) {
		super();
		this.idPlyta = idPlyta;
		this.tytul = tytul;
		this.rezyser = rezyser;
		this.gatunek = gatunek;
		this.cena = cena;
	}

	public Plyta(String tytul, Rezyser rezyser, Gatunek gatunek, double cena) {
		super();
		this.tytul = tytul;
		this.rezyser = rezyser;
		this.gatunek = gatunek;
		this.cena = cena;
	}
	
	public Plyta () {
	}
	
	public int getIdPlyta() {
		return idPlyta;
	}
	public void setIdPlyta(int idPlyta) {
		this.idPlyta = idPlyta;
	}
	public String getTytul() {
		return tytul;
	}
	public void setTytul(String tytul) {
		this.tytul = tytul;
	}
	public Rezyser getRezyser() {
		return rezyser;
	}
	public void setRezyser(Rezyser rezyser){
		this.rezyser = rezyser;
	}
	public Gatunek getGatunek() {
		return gatunek;
	}
	public void setGatunek(Gatunek gatunek) {
		this.gatunek = gatunek;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}

	@Override
	public String toString() {
		return tytul;
	}
	
	
	
	
}
