package sql;

public class Rezyser {

	private int idRezyser;
	private String imie;
	private String nazwisko;
	
	
	
	public Rezyser(String imie, String nazwisko) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
	}
	
	
	public Rezyser() {
		super();
	}


	public int getIdRezyser() {
		return idRezyser;
	}
	public void setIdRezyser(int idRezyser) {
		this.idRezyser = idRezyser;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	@Override
	public String toString() {
		return  imie + " " + nazwisko;
	}
	
	
	
	
}
