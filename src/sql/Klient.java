package sql;

public class Klient {
	private int idKlient;
	private String imie;
	private String nazwisko;
	private String miasto;
	
	public Klient(String imie, String nazwisko, String miasto) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.miasto = miasto;
	}
	
	
	public Klient() {
		super();
	}

	public int getIdKlient() {
		return idKlient;
	}
	public void setIdKlient(int idKlient) {
		this.idKlient = idKlient;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getMiasto() {
		return miasto;
	}
	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}
	
	
}
