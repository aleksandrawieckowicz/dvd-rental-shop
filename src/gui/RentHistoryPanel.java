package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTable;

import sql.Klient;
import sql.Wypozyczenie;

public class RentHistoryPanel extends JPanel {
	
	private JTable table;
	public static final int COLUMN_SIZE = 6;
	private String[] columnName;

	public RentHistoryPanel (List<Wypozyczenie> listOfWypozyczenie) {
		
		setLayout(new GridBagLayout());
		
		columnName = new String[6];
		columnName[0] = "Numer wypożyczenia";
		columnName[1] = "Numer egzemplarza";
		columnName[2] = "Numer klienta";
		columnName[3] = "Data wypożyczenia";
		columnName[4] = "Data zwrotu";
		columnName[5] = "Status";
		
		table = new JTable(getTabOfKlient(listOfWypozyczenie), columnName);
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		add(table.getTableHeader(), c);
		
		c.gridx = 0;
		c.gridy = 1;
		add(table, c);
		
	}
	
	private String[][] getTabOfKlient(List<Wypozyczenie> listOfWypozyczenie) {
		String[][] tableOfWypozyczenie = new String[listOfWypozyczenie.size()][COLUMN_SIZE];
		int i = 0;
		for (Wypozyczenie x : listOfWypozyczenie) {
			tableOfWypozyczenie[i][0] = String.valueOf(x.getIdWypozyczenie());
			tableOfWypozyczenie[i][1] = String.valueOf(x.getIdEgzemplarz());
			tableOfWypozyczenie[i][2] = String.valueOf(x.getIdKlient());
			tableOfWypozyczenie[i][3] = x.makeDateFormat(x.getDataWypozyczenia());
			tableOfWypozyczenie[i][4] = x.makeDateFormat(x.getDataZwrotu());
			tableOfWypozyczenie[i][5] = x.getStatus();
			i++;
		}
		return tableOfWypozyczenie;
	}
}
