package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dao.impl.RezyserDAOImpl;
import sql.Rezyser;

public class AddDirectorPanel extends JPanel {

	private JLabel addDirectorlabel;
	private JLabel firstNameLabel;
	private JLabel secondNameLabel;
	private JButton addButton;
	private JTextField writeFirstName;
	private JTextField writeSecondName;
	
	private final RezyserDAOImpl rezyserDAOImpl = new RezyserDAOImpl();;
	private MainWindowPanel mainWindowPanel;


	public AddDirectorPanel(MainWindowPanel panel) {
		// parametr w konstruktorze oraz to poni�ej jest po to by odwo�a� si� do okna, na kt�rym stawia si� ten panel by je zamkn��, poni�ej jest odwo�anie do tego okna
		this.mainWindowPanel = panel;
		
		this.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		addDirectorlabel = new JLabel("DODAJ RE�YSERA");
		c.gridx = 0;
		c.gridy = 0;
		add(addDirectorlabel, c);

		firstNameLabel = new JLabel("Podaj imi�");
		c.gridx = 0;
		c.gridy = 1;
		add(firstNameLabel, c);

		secondNameLabel = new JLabel("Podaj nazwisko");
		c.gridx = 0;
		c.gridy = 3;
		add(secondNameLabel, c);

		writeFirstName = new JTextField();
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 2;
		c1.gridwidth = 2;
		add(writeFirstName, c1);

		writeSecondName = new JTextField();
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 4;
		c2.gridwidth = 2;
		add(writeSecondName, c2);
		
		addButton = new JButton("Dodaj");
		c.gridx = 1;
		c.gridy = 5;
		add(addButton, c);
		
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Rezyser rezyser = new Rezyser(writeFirstName.getText(), writeSecondName.getText());
				
				try {
					rezyserDAOImpl.insertIntoRezyser(rezyser);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				setVisible(false);
				//Zamyka okno (wczesniej zamykal sie sam panel)
				mainWindowPanel.getAddDirectorWindow().setVisible(false);
				panel.setVisible(true);
			}
		});

	}

}
