package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;

import dao.impl.WypozyczenieDAOImpl;
import oracle.security.crypto.core.Padding.ID;
import sql.Wypozyczenie;

public class FindRentHistoryPanel extends JPanel {
	
	JTable table;
	JLabel label;
	JTextField textField;
	JRadioButton egzemplarzButton;
	JRadioButton klientButton;
	JButton findButton;
	JButton seeAllButton;
	WypozyczenieDAOImpl wypozyczenieDAOImpl;
	List<Wypozyczenie> listOfWypozyczenie;
	
	public FindRentHistoryPanel() {
		
		wypozyczenieDAOImpl = new WypozyczenieDAOImpl();
		
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints c1 = new GridBagConstraints();
		
		label = new JLabel("Podaj ID czytelnika lub ID egzemplarza");
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth = 3;
		add(label, c1);
		
		GridBagConstraints c3 = new GridBagConstraints();
		textField = new JTextField();
		c3.fill = GridBagConstraints.HORIZONTAL;
		c3.gridx = 0;
		c3.gridy = 1;
		c3.gridwidth = 1;
		add(textField, c3);
		
		GridBagConstraints c2 = new GridBagConstraints();
		
		egzemplarzButton = new JRadioButton("egzemplarz");
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 2;
		add(egzemplarzButton, c2);
		
		klientButton = new JRadioButton("klient");
		c2.gridx = 1;
		c2.gridy = 2;
		add(klientButton, c2);
		
		findButton = new JButton("Szukaj");
		c2.gridx = 0;
		c2.gridy = 3;
		add(findButton, c2);
		findButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(egzemplarzButton.isSelected() && !klientButton.isSelected()) {
					try {
						listOfWypozyczenie = wypozyczenieDAOImpl.getReturnedWypozyczenieByEgzemplarz(Integer.valueOf((textField.getText())));
						Window window = new Window("Historia wypożyczeń", 3000, 2000, new RentHistoryPanel(listOfWypozyczenie), Window.DISPOSE_ON_CLOSE);
					} catch (NumberFormatException | SQLException e1) {
						e1.printStackTrace();
					}
				} else if (klientButton.isSelected() && !egzemplarzButton.isSelected()){
					try {
						listOfWypozyczenie = wypozyczenieDAOImpl.getReturnedWypozyczenieByKlient(Integer.valueOf(textField.getText()));
						Window window = new Window("Historia wypożyczeń", 3000, 2000, new RentHistoryPanel(listOfWypozyczenie), Window.DISPOSE_ON_CLOSE);
					} catch (NumberFormatException | SQLException e1) {
						e1.printStackTrace();
					}
				}	
			}
		});
		
		seeAllButton = new JButton("Wyświetl wszystko");
		c2.gridx = 1;
		c2.gridy = 3;
		add(seeAllButton, c2);
		seeAllButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					listOfWypozyczenie = wypozyczenieDAOImpl.getAllReturnedWypozyczenie();
					Window window = new Window("Historia wypożyczeń", 3000, 2000, new RentHistoryPanel(listOfWypozyczenie), Window.DISPOSE_ON_CLOSE);
				} catch (SQLException e1) {

					e1.printStackTrace();
				}
				
				
			}
		});
		
	}
}
