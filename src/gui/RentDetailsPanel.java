package gui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import dao.WypozyczenieDAO;
import dao.impl.WypozyczenieDAOImpl;
import sql.Wypozyczenie;

public class RentDetailsPanel extends JPanel {
	
	JLabel rentDetail;
	JTable table;
	String[] columnName;
	String[][] tabOfWypoz;
	private SimpleDateFormat sdf;
	String rentDate;
	String returnDate;
	JButton returnButton;
	WypozyczenieDAO wypozyczenieDAO;
	
	public RentDetailsPanel(Wypozyczenie wypozyczenie) {
		
		this.setLayout(new GridBagLayout());
		
		columnName = new String[6];
		columnName[0] = "Numer wypo�yczenia";
		columnName[1] = "Numer egzemplarza";
		columnName[2] = "Numer klienta";
		columnName[3] = "Data wypo�yczenia";
		columnName[4] = "Data zwrotu";
		columnName[5] = "Kara";
		
		tabOfWypoz = new String[1][columnName.length];
		tabOfWypoz[0][0] = String.valueOf(wypozyczenie.getIdWypozyczenie());
		tabOfWypoz[0][1] = String.valueOf(wypozyczenie.getIdEgzemplarz());
		tabOfWypoz[0][2] = String.valueOf(wypozyczenie.getIdKlient());
		tabOfWypoz[0][3] = String.valueOf(wypozyczenie.makeDateFormat(wypozyczenie.getDataWypozyczenia()));
		if (!(wypozyczenie.getDataZwrotu() == 0)) {
		tabOfWypoz[0][4] = String.valueOf(wypozyczenie.makeDateFormat(wypozyczenie.getDataZwrotu()));
		} else {
			tabOfWypoz[0][4] = String.valueOf(wypozyczenie.getDataZwrotu());
		}
		
		double daysBetween = Math.round((wypozyczenie.getDataZwrotu() - wypozyczenie.getDataWypozyczenia())/(1000*86400));
		// po 30 dniach niezwr�cenia ksi��ki naliczana jest kara wysoko�ci 0.5 z� za ka�dy dzie�
		if (daysBetween > 30) {
			double kara = (daysBetween - 30)*0.5;
			wypozyczenie.setKara(kara);
			try {
				wypozyczenieDAO.insertKara(wypozyczenie);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		tabOfWypoz[0][5] = String.valueOf(wypozyczenie.getKara());
	
		table = new JTable(tabOfWypoz, columnName);
		
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth = 3;
		add(table.getTableHeader(), c1);
		
		c1.gridx = 0;
		c1.gridy = 1;
		add(table, c1);
		table.setRowHeight(20);
		resizeColumnWidh(table);
		
		returnButton = new JButton("Zwr��");
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 2;
		add(returnButton, c2);
		returnButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				long returnDate = System.currentTimeMillis();
				wypozyczenie.setDataZwrotu(returnDate);
				wypozyczenie.setStatus("zwr�cony");
				wypozyczenieDAO = new WypozyczenieDAOImpl();
				try {
					wypozyczenieDAO.changeStatus(wypozyczenie);
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
				
				try {
					wypozyczenieDAO.insertDataZwrotu(wypozyczenie);
					wypozyczenieDAO.changeStatus(wypozyczenie);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				LabelPanel returnedPanel = new LabelPanel("Zwr�cono");
				Window window = new Window("", 500, 300, returnedPanel, Window.DISPOSE_ON_CLOSE);
			}
		});
		

;	}
	
	public void resizeColumnWidh(JTable table2) {
		for (int column = 0; column < table2.getColumnCount(); column++) {
			int width = 120; // Min width
			for (int row = 0; row < table2.getRowCount(); row++) {
				TableCellRenderer renderer = table2.getCellRenderer(row, column);
				Component comp = table2.prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width + 1, width);
			}
			if (width > 300)
				width = 300;
			table2.getColumnModel().getColumn(column).setPreferredWidth(width);
		}
	
	}	
}
