package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dao.GatunekDAO;
import dao.impl.GatunekDAOImpl;
import sql.Gatunek;
import sql.Rezyser;

public class AddTypePanel extends JPanel {
	
	private JLabel addTypelabel;
	private JLabel nameLabel;
	private JButton addButton;
	private JTextField writeType;
	private MainWindowPanel MainWindowPanel;
	private final GatunekDAO gatunekDAOImpl = new GatunekDAOImpl();

	
	public AddTypePanel(MainWindowPanel panel){
		this.MainWindowPanel = panel;
		
		this.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		addTypelabel = new JLabel("DODAJ GATUNEK FILMU");
		c.gridx = 0;
		c.gridy = 0;
		add(addTypelabel, c);

		nameLabel = new JLabel("Podaj nazw� gatunku");
		c.gridx = 0;
		c.gridy = 1;
		add(nameLabel, c);
		
		writeType = new JTextField();
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 2;
		c1.gridwidth = 2;
		add(writeType, c1);
		
		addButton = new JButton("Dodaj");
		c.gridx = 1;
		c.gridy = 3;
		add(addButton, c);
		
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Gatunek gatunek = new Gatunek(writeType.getText());
				
				try {
					gatunekDAOImpl.insertIntoGatunek(gatunek);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				setVisible(false);
				MainWindowPanel.getAddTypeWindow().setVisible(false);
				
			}
		});
		
		
	}

}
