package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dao.KlientDAO;
import dao.impl.KlientDAOImpl;
import sql.Klient;

public class AddKlientPanel extends JPanel{
	
	private JLabel addKlient;
	private JLabel addName;
	private JLabel addSurname;
	private JLabel addCity;
	private JTextField writeName;
	private JTextField writeSurname;
	private JTextField writeCity;
	private JButton addButton;
	private MainWindowPanel mainWindowPanel;
	private KlientDAO klientDAO=  new KlientDAOImpl();
		
	public AddKlientPanel(MainWindowPanel panel) {
		this.mainWindowPanel = panel;
		
		this.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		addKlient = new JLabel("DODAJ KLIENTA");
		c.gridx = 0;
		c.gridy = 0;
		add(addKlient, c);
		
		addName = new JLabel("Dodaj imi�");
		c.gridx = 0;
		c.gridy = 1;
		add(addName, c);
		
		writeName = new JTextField();
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 2;
		c1.gridwidth =2;
		add(writeName, c1);
		
		addSurname = new JLabel("Dodaj nazwisko");
		c.gridx = 0;
		c.gridy = 3;
		add(addSurname, c);
		
		writeSurname = new JTextField();
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 4;
		c2.gridwidth = 2;
		add(writeSurname, c2);
		
		addCity = new JLabel("Dodaj miasto");
		c.gridx = 0;
		c.gridy = 5;
		add(addCity, c);
		
		writeCity = new JTextField();
		GridBagConstraints c3 = new GridBagConstraints();
		c3.fill = GridBagConstraints.HORIZONTAL;
		c3.gridx = 0;
		c3.gridy = 6;
		c3.gridwidth = 2;
		add(writeCity, c3);
	
		addButton = new JButton("DODAJ");
		c1.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 7;
		add(addButton, c);
		
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Klient klient = new Klient(writeName.getText(), writeSurname.getText(), writeCity.getText());
				
				try {
					klientDAO.insertIntoKlient(klient);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				setVisible(false);
				mainWindowPanel.getAddKlientWindow().setVisible(false);
				
			}
		});
		
	}

	
}
