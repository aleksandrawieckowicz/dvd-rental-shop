package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class SearchDeleteReturnPanel extends JPanel {
	
	JButton rentButton;
	JButton returnButton;
	JButton deleteButton;
	
	public SearchDeleteReturnPanel(){
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		rentButton = new JButton("Wypo�ycz");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		add(rentButton, c);
		
		returnButton = new JButton("Zwr��");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		add(returnButton, c);
		
		deleteButton = new JButton("Usu�");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 0;
		add(deleteButton, c);
		
		
	}
	
	

}
