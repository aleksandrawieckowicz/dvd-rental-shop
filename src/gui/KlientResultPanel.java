package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import dao.impl.KlientDAOImpl;
import sql.Klient;

public class KlientResultPanel extends JPanel {

	private static final long serialVersionUID = 4901707677074366387L;
	
	public static final int COLUM_SIZE = 4;
	
	private JButton selectButton;
	public String[] columnName;
	JTable table;
	KlientDAOImpl klientDAOImpl;
	String select;
	String[][] tabOfKlient;
	List<Klient> resultOfQuestion = new ArrayList();
	ActionListener selectToRent;
	ActionListener selectToshowRents;

	public KlientResultPanel(List<Klient> resultOfQuestionForKlient, ActionListener actionListener) throws SQLException {
		
		tabOfKlient = getTabOfKlient(resultOfQuestionForKlient);
		this.setLayout(new GridBagLayout());
		this.setPreferredSize(new Dimension(200, 200));
		this.setMinimumSize(new Dimension(200, 200));
		columnName = new String[COLUM_SIZE];
		columnName[0] = "ID";
		columnName[1] = "Imi�";
		columnName[2] = "Nazwisko";
		columnName[3] = "Miasto";
		
		table = new JTable(tabOfKlient, columnName);
		
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth =3;
		add(table.getTableHeader(), c1);

		c1.gridx = 0;
		c1.gridy = 1;
		add(table, c1);
		table.setRowHeight(20);
		resizeColumnWidh(table);
		
		selectButton = new JButton("Wybierz");
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 2;
		c2.gridwidth = 1;
		add(selectButton, c2);
		selectButton.addActionListener(actionListener);

		table.setFillsViewportHeight(true);
	}
	
	private String[][] getTabOfKlient(List<Klient> listOfKlient) {
		String[][] tableOfKlient = new String[listOfKlient.size()][COLUM_SIZE];
		int i = 0;
		for (Klient x : listOfKlient) {
			tableOfKlient[i][0] = String.valueOf(x.getIdKlient());
			tableOfKlient[i][1] = x.getImie();
			tableOfKlient[i][2] = x.getNazwisko();
			tableOfKlient[i][3] = x.getMiasto();
			i++;
		}
		return tableOfKlient;
	}

	/**
	 * dopasowuje szeroko�� wiersza do wype�nienia
	 * 
	 * @param table
	 */
	public void resizeColumnWidh(JTable table) {
		for (int column = 0; column < table.getColumnCount(); column++) {
			int width = 15; // Min width
			for (int row = 0; row < table.getRowCount(); row++) {
				TableCellRenderer renderer = table.getCellRenderer(row, column);
				Component comp = table.prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width + 1, width);
			}
			if (width > 100)
				width = 100;
			table.getColumnModel().getColumn(column).setPreferredWidth(width);
		}

	}

}
