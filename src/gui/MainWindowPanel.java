package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import dao.WypozyczenieDAO;
import dao.impl.KlientDAOImpl;
import dao.impl.PlytaDAOImpl;
import dao.impl.WypozyczenieDAOImpl;
import sql.Klient;
import sql.Plyta;
import sql.Wypozyczenie;

public class MainWindowPanel extends JPanel {

	private final JLabel label1;
	private JTextField findByText;
	private JRadioButton plyta;
	private JRadioButton klient;
	private JButton find;
	private JButton selectAll;
	private JButton addKlient;
	private JButton addPlyta;
	private JButton addRezyser;
	private JButton addGatunek;
	private Window addDirectorWindow;
	private Window addTypeWindow;
	private Window addPlytaWindow;
	private Window addKlientWindow;
	private Window resultKlientWindow;
	public Window resultPlytaWindow;
	public JButton historyOfRent;
	KlientDAOImpl klientDAOImpl;

	private KlientResultPanel klientResultPanel;
	private PlytaResultPanel plytaResultPanel;
	public List<Klient> resultOfQuestionForKlient;
	public List<Plyta> resultOfQuestionForPlyta;
	private String[][] tabOfKlient;
	private String[][] tabOfPlyta;
	private PlytaDAOImpl plytaDAOImpl;

	public MainWindowPanel() {
		this.setLayout(new GridBagLayout());
		klientDAOImpl = new KlientDAOImpl();
		plytaDAOImpl = new PlytaDAOImpl();

		GridBagConstraints c = new GridBagConstraints();
		GridBagConstraints c1 = new GridBagConstraints();
		// c.fill = GridBagConstraints.HORIZONTAL;

		label1 = new JLabel("Wyszukiwanie");
		// c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		add(label1, c);

		findByText = new JTextField();
		c1.gridx = 0;
		c1.gridy = 2;
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridwidth = 2;

		add(findByText, c1);

		plyta = new JRadioButton("p�yta");
		// c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 3;
		add(plyta, c);

		klient = new JRadioButton("klient");
		// c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 3;
		add(klient, c);

		find = new JButton("szukaj");
		// c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 4;
		add(find, c);

		find.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (klient.isSelected()) {

					try {
						resultOfQuestionForKlient = klientDAOImpl.findKlient(findByText.getText());
						klientResultPanel = new KlientResultPanel(resultOfQuestionForKlient, new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								// index zaznaczonego klienta w KlientResultPanel (nast�pny panel)
								int indexOfSelectedKlient = klientResultPanel.table.getSelectedRow();
								int idOfSelectedKlient = resultOfQuestionForKlient.get(indexOfSelectedKlient).getIdKlient();
								WypozyczenieDAO wypozyczenieDAO = new  WypozyczenieDAOImpl();
								Wypozyczenie wypozyczenie;
								try {
									wypozyczenie = wypozyczenieDAO.getWypozyczenieByKlient(idOfSelectedKlient).get(0);
									new RentDetailsPanel(wypozyczenie);
								} catch (SQLException e) {
									e.printStackTrace();
								} catch (IndexOutOfBoundsException e) {			
									Window window = new Window(" ", 200, 100, new LabelPanel("Klient nie ma wypo�ycze�"), Window.DISPOSE_ON_CLOSE);
								}
								
								
							}
						});
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					resultKlientWindow = new Window("Klienci", 3000, 3000, klientResultPanel, Window.DISPOSE_ON_CLOSE);

				} else if (plyta.isSelected()) {
					try {
						resultOfQuestionForPlyta = plytaDAOImpl.findPlyta(findByText.getText());
						plytaResultPanel = new PlytaResultPanel(resultOfQuestionForPlyta);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					resultPlytaWindow = new Window("P�yty", 3000, 3000, plytaResultPanel, Window.DISPOSE_ON_CLOSE);
				}

			}
		});

		selectAll = new JButton("Wy�wietl wszystko");
		// c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 4;

		add(selectAll, c);
		selectAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (klient.isSelected()) {
					try {
						resultOfQuestionForKlient = klientDAOImpl.getAllKlient();
						klientResultPanel = new KlientResultPanel(resultOfQuestionForKlient, new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								// TODO Auto-generated method stub
								
							}
						});
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					resultKlientWindow = new Window("Klienci", 2000, 2000, klientResultPanel, Window.DISPOSE_ON_CLOSE);
				} else if (plyta.isSelected()) {
					try {
						resultOfQuestionForPlyta = plytaDAOImpl.getAllPlyta();
						plytaResultPanel = new PlytaResultPanel(resultOfQuestionForPlyta);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					resultPlytaWindow = new Window("P�yty", 2000, 2000, plytaResultPanel, Window.DISPOSE_ON_CLOSE);
				}

			}
		});

		addPlyta = new JButton("Dodaj p�yt�");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 4;
		c.gridy = 0;
		add(addPlyta, c);

		addPlyta.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					addPlytaWindow = new Window("Dodaj DVD", 2000, 2000, new AddPlytaPanel(getThis()),
							Window.DISPOSE_ON_CLOSE);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		addKlient = new JButton("Dodaj klienta");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 4;
		c.gridy = 1;
		add(addKlient, c);

		addKlient.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				addKlientWindow = new Window("Dodaj klienta", 2000, 2000, new AddKlientPanel(getThis()),
						Window.DISPOSE_ON_CLOSE);

			}
		});

		addRezyser = new JButton("Dodaj re�ysera");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 4;
		c.gridy = 2;
		add(addRezyser, c);

		addRezyser.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addDirectorWindow = new Window("Dodaj re�ysera", 2000, 2000, new AddDirectorPanel(getThis()),
						Window.DISPOSE_ON_CLOSE);
			}
		});

		addGatunek = new JButton("Dodaj gatunek");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 4;
		c.gridy = 3;
		add(addGatunek, c);

		addGatunek.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addTypeWindow = new Window("Dodaj gatunek", 2000, 2000, new AddTypePanel(getThis()),
						Window.DISPOSE_ON_CLOSE);
			}
		});
		
		historyOfRent = new JButton("Historia wypo�ycze�");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 4;
		c.gridy = 4;
		add(historyOfRent, c);
		historyOfRent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Window window = new Window("", 3000, 2000, new FindRentHistoryPanel(), Window.DISPOSE_ON_CLOSE);
			}
		});
	}

	// Zwraca okno w klasie AddRezyserPanel
	public Window getAddDirectorWindow() {
		return this.addDirectorWindow;
	}

	// Zwraca okno w klasie AddGatunekPanel
	public Window getAddTypeWindow() {
		return this.addTypeWindow;
	}

	// Zwraca okno w klasie AddPlytaPanel
	public Window getAddPlytaWindow() {
		return this.addPlytaWindow;
	}

	// Zwraca okno w klasie AddKlientPanel
	public Window getAddKlientWindow() {
		return this.addKlientWindow;
	}

	// Zwraca okno w klasie ResultKlientPanel
	public Window getResultKlientWindow() {
		return this.resultKlientWindow;
	}

	private MainWindowPanel getThis() {
		return this;
	}

	

}
