package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dao.GatunekDAO;
import dao.PlytaDAO;
import dao.RezyserDAO;
import dao.impl.GatunekDAOImpl;
import dao.impl.PlytaDAOImpl;
import dao.impl.RezyserDAOImpl;
import sql.Gatunek;
import sql.Plyta;
import sql.Rezyser;

public class AddPlytaPanel extends JPanel {

	private JLabel addPlytalabel;
	private JLabel tittleLabel;
	private JLabel priceLabel;
	private JLabel directorLabel;
	private JLabel typeLabel;
	private JButton addButton;
	private JTextField writeTittle;
	private JTextField writePrice;
	private JComboBox<Rezyser> selectDirector;
	private JComboBox<Gatunek> selectType;
	private MainWindowPanel mainWindowPanel;
	private GatunekDAO gatunekDAO = new GatunekDAOImpl();
	private RezyserDAO rezyserDAO = new RezyserDAOImpl();
	private PlytaDAO plytaDAO = new PlytaDAOImpl();

	public AddPlytaPanel(MainWindowPanel panel) throws SQLException {
		this.mainWindowPanel = panel;

		this.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		addPlytalabel = new JLabel("DODAJ P�YT�");
		c.gridx = 0;
		c.gridy = 0;
		add(addPlytalabel, c);

		tittleLabel = new JLabel("Podaj tytu�");
		c.gridx = 0;
		c.gridy = 1;
		add(tittleLabel, c);

		priceLabel = new JLabel("Podaj cen�");
		c.gridx = 0;
		c.gridy = 3;
		add(priceLabel, c);

		directorLabel = new JLabel("Wybierz re�ysera");
		c.gridx = 0;
		c.gridy = 5;
		add(directorLabel, c);

		typeLabel = new JLabel("Wybierz gatunek");
		c.gridx = 0;
		c.gridy = 7;
		add(typeLabel, c);

		writeTittle = new JTextField();
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 2;
		c1.gridwidth = 2;
		add(writeTittle, c1);

		writePrice = new JTextField();
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 4;
		c2.gridwidth = 2;
		add(writePrice, c2);

		selectDirector = new JComboBox<Rezyser>();
		GridBagConstraints c3 = new GridBagConstraints();
		c3.fill = GridBagConstraints.HORIZONTAL;
		c3.gridx = 0;
		c3.gridy = 6;
		c3.gridwidth = 2;
		add(selectDirector, c3);

		List<Rezyser> listOfDirector = rezyserDAO.getAllRezyser();
		for (Rezyser x : listOfDirector) {
			selectDirector.addItem(x);
		}

		selectType = new JComboBox<Gatunek>();
		GridBagConstraints c4 = new GridBagConstraints();
		c4.fill = GridBagConstraints.HORIZONTAL;
		c4.gridx = 0;
		c4.gridy = 8;
		c4.gridwidth = 2;
		add(selectType, c4);

		List<Gatunek> listOfGatunek = gatunekDAO.getAllGatunek();
		for (Gatunek x : listOfGatunek) {
			selectType.addItem(x);
		}

		addButton = new JButton("Dodaj");
		c.gridx = 1;
		c.gridy = 9;
		add(addButton, c);

		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Plyta plyta = new Plyta(writeTittle.getText(),
						((Rezyser) selectDirector.getSelectedItem()),
						((Gatunek) selectType.getSelectedItem()),
						Double.parseDouble(writePrice.getText()));
				try {
					plytaDAO.insertIntoPlyta(plyta);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				setVisible(false);
				mainWindowPanel.getAddPlytaWindow().setVisible(false);
			}
			// zmapowa�, �eby wy�ej pobiera�o rezysera i gatunek jako dane z bazy z id a nie
			// string od razu - tylko potem
			// i combobox ma by� nie ze strigiem tylko z np.gatunkiem, rezyserem

		});

	}

}
