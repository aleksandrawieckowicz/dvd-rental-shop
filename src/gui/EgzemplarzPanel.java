package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.table.TableCellRenderer;

import dao.impl.EgzemplarzDAOImpl;
import dao.impl.WypozyczenieDAOImpl;
import sql.Egzemplarz;
import sql.Plyta;
import sql.Wypozyczenie;

public class EgzemplarzPanel extends JPanel {

	JTable table;
	JButton addButton;
	JButton rentButton;
	JButton detailsButton;
	public static final int COLUMN_SIZE = 2;
	public String[] columnName;
	EgzemplarzDAOImpl egzemplarzDAOImpl = new EgzemplarzDAOImpl();
	public String[][] tableOfEgzemplarz;
	JLabel labelAdd;

	int indexOfselectedPlyta;
	Plyta selectedPlyta;
	JTable tabPanel;
	Window rentDetailsWindow;
	RentDetailsPanel rentDetailsPanel;
	Egzemplarz selectedEgzemplarz;
	List<Egzemplarz> listOfEgzemplarz;
	WypozyczenieDAOImpl wypozyczenieDAOImpl;
	Wypozyczenie wypozyczenie;
	Plyta plyta;
	
	public EgzemplarzPanel(Plyta plyta, PlytaResultPanel plytaResultPanel) throws SQLException {
		this.plyta = plyta;
		listOfEgzemplarz = egzemplarzDAOImpl.showEgzemplarz(plyta.getIdPlyta());
		tableOfEgzemplarz = getTabOfEgzemplarz(listOfEgzemplarz);
		

		columnName = new String[COLUMN_SIZE];
		columnName[0] = "ID egzemplarz";
		columnName[1] = "Stan";

		table = new JTable(tableOfEgzemplarz, columnName);
		this.setLayout(new GridBagLayout());
		
		

		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth = 3;
		add(table.getTableHeader(), c1);

		c1.gridx = 0;
		c1.gridy = 1;
		add(table, c1);
		table.setRowHeight(20);
		resizeColumnWidh(table);

		addButton = new JButton("Dodaj");
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 2;
		add(addButton, c2);
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					egzemplarzDAOImpl.addEgzemplarz(plyta);
					Window window = new Window("", 200, 100, new LabelPanel("Dodano"), Window.DISPOSE_ON_CLOSE, plytaResultPanel.getResultEgzemplarzWindow());
				} catch (SQLException e1) {
				}
			}
		});

		rentButton = new JButton("Wypo�ycz");
		c2.gridx = 1;
		c2.gridy = 2;
		add(rentButton, c2);
		rentButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(tableOfEgzemplarz[table.getSelectedRow()][1] == "dost�pny") {
				selectedEgzemplarz = listOfEgzemplarz.get(table.getSelectedRow());
				new Window("", 3000, 2000, new FindKlientPanel(getThis()), Window.DISPOSE_ON_CLOSE);
				} else {
					Window window = new Window(" ", 3000, 1000, new LabelPanel("Egzemplarz jest wypo�yczony"), Window.DISPOSE_ON_CLOSE);
				}
			}
		});

		detailsButton = new JButton("Szczeg�y");
		c2.gridx = 2;
		c2.gridy = 2;
		add(detailsButton, c2);
		detailsButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				wypozyczenieDAOImpl = new WypozyczenieDAOImpl();
				// Index = 0, poniewa� wyszukuj�c po egzemplarzu w aktualnych wypozyczeniach b�dzie tylko 1 wynik
				try { 
					selectedEgzemplarz = listOfEgzemplarz.get(table.getSelectedRow());
					wypozyczenie = (wypozyczenieDAOImpl.getWypozyczenie(selectedEgzemplarz.getIdEgzemplarz())).get(0);
				} catch (SQLException e1) {
					e1.printStackTrace();
				} 
				rentDetailsPanel = new RentDetailsPanel(wypozyczenie);
				rentDetailsWindow = new Window("Informacje o wypo�yczeniu", 2000,1000, rentDetailsPanel, Window.DISPOSE_ON_CLOSE);
				
			}
		});
	}
	
	private EgzemplarzPanel getThis() {
		return this;
	}

	private String[][] getTabOfEgzemplarz(List<Egzemplarz> list) throws SQLException {
		String[][] tabOfEgzemplarz = new String[list.size()][COLUMN_SIZE];
		int i = 0;
		for (Egzemplarz x : list) {
			tabOfEgzemplarz[i][0] = String.valueOf(x.getIdEgzemplarz());
			tabOfEgzemplarz[i][1] = availabillityOfEgzemplarz(x);
			i++;
		}
		return tabOfEgzemplarz;

	}
	
	public String availabillityOfEgzemplarz (Egzemplarz egzemplarz) throws SQLException{
		if (egzemplarzDAOImpl.isEgzemplarzAvailabe(egzemplarz.getIdEgzemplarz()) == null) {
		return "dost�pny";
		} else {
			return "wypo�yczony";
		}
		
	}

	public void resizeColumnWidh(JTable table2) {
		for (int column = 0; column < table2.getColumnCount(); column++) {
			int width = 100; // Min width
			for (int row = 0; row < table2.getRowCount(); row++) {
				TableCellRenderer renderer = table2.getCellRenderer(row, column);
				Component comp = table2.prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width + 1, width);
			}
			if (width > 300)
				width = 300;
			table2.getColumnModel().getColumn(column).setPreferredWidth(width);
		}

	}
}
