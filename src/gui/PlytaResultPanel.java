package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import dao.impl.PlytaDAOImpl;
import sql.Plyta;

public class PlytaResultPanel extends JPanel {
	private static final long serialVersionUID = 4901707677074366387L;
public static final int COLUM_SIZE = 4;
	
	public JButton selectButton;
	public String[] columnName;
	public JTable table;
	PlytaDAOImpl PlytaDAOImpl;
	String select;
	String[][] tabOfPlyta;
	List<Plyta> resultOfQuestion = new ArrayList();
	EgzemplarzPanel egzemplarzPanel;
	MainWindowPanel mainWindowPanel;
	Window resultEgzemplarzWindow;
	PlytaDAOImpl plytaDAOImpl;
	List<Plyta> resultOfQuestionForPlyta;
	
	PlytaResultPanel() {
		super();
	}

	public PlytaResultPanel(List<Plyta> resultOfQuestionForPlyta) throws SQLException {
		
		this.resultOfQuestionForPlyta = resultOfQuestionForPlyta;
		tabOfPlyta = getTabOfPlyta(resultOfQuestionForPlyta);
	
		//this.setLayout(new GridBagLayout());
		//this.setPreferredSize(new Dimension(400, 120));
		//this.setMinimumSize(new Dimension(400, 120));
		columnName = new String[COLUM_SIZE];
		columnName[0] = "Tytu�";
		columnName[1] = "Re�yser";
		columnName[2] = "Gatunek";
		columnName[3] = "Cena";
		
		this.setLayout(new GridBagLayout());
		
		JTable table = new JTable(tabOfPlyta, columnName);
		
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth =3;
		add(table.getTableHeader(), c1);

		c1.gridx = 0;
		c1.gridy = 1;
		add(table, c1);
		table.setRowHeight(20);
		resizeColumnWidh(table);
		
		selectButton = new JButton("Wybierz");
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 2;
		c2.gridwidth = 1;
		add(selectButton, c2);
		
		table.setFillsViewportHeight(true);
		selectButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					egzemplarzPanel = new EgzemplarzPanel(resultOfQuestionForPlyta.get(table.getSelectedRow()), getThis());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				resultEgzemplarzWindow = new Window("Egzemplarze" ,2000, 2000, egzemplarzPanel, Window.DISPOSE_ON_CLOSE);
			}
		});		
	}
	
	
	
	public Window getResultEgzemplarzWindow() {
		return resultEgzemplarzWindow;
	}
	

	public PlytaResultPanel getThis() {
		return this;
	}
	
	private String[][] getTabOfPlyta(List<Plyta> listOfPlyta) {
		String[][] tableOfPlyta = new String[listOfPlyta.size()][PlytaResultPanel.COLUM_SIZE];
		int i = 0;
		for (Plyta x : listOfPlyta) {
			tableOfPlyta[i][0] = String.valueOf(x.getTytul());
			tableOfPlyta[i][1] = x.getRezyser().getImie() + " " + x.getRezyser().getNazwisko();
			tableOfPlyta[i][2] = x.getGatunek().getNazwaGatunek();
			tableOfPlyta[i][3] = String.valueOf(x.getCena());
			i++;
		}
		return tableOfPlyta;
	}
	
	/**
	 * dopasowuje szeroko�� wiersza do wype�nienia
	 * 
	 * @param table
	 */
	public void resizeColumnWidh(JTable table) {
		for (int column = 0; column < table.getColumnCount(); column++) {
			int width = 15; // Min width
			for (int row = 0; row < table.getRowCount(); row++) {
				TableCellRenderer renderer = table.getCellRenderer(row, column);
				Component comp = table.prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width + 1, width);
			}
			if (width > 100)
				width = 100;
			table.getColumnModel().getColumn(column).setPreferredWidth(width);
		}

	}

}


