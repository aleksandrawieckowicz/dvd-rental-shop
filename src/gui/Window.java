package gui;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Window extends JFrame {

	private Window window;

	public Window(String tytul, int width, int height, JPanel panel, int whatOnClose) {
		super(tytul);
		setDefaultCloseOperation(whatOnClose);
		setVisible(true);
		setSize(width, height);
		setLocation(50, 50);
		add(panel);
		setResizable(true);
		pack();

	}

	public Window(String tytul, int width, int height, JPanel panel, int whatOnClose, Window window) {
		super(tytul);
		this.window = window;
		setDefaultCloseOperation(whatOnClose);
		setVisible(true);
		setSize(width, height);
		setLocation(50, 50);
		add(panel);
		setResizable(true);
		pack();
		window.dispose();

	}

	@Override
	public void dispose() {
		super.dispose();
		if (window != null) {
			window.dispose();
		}
	}

	public Window(String tytul, int width, int height, JPanel panel1, JPanel panel2, int whatOnClose) {
		super(tytul);
		JPanel window = new JPanel();
		setDefaultCloseOperation(whatOnClose);
		setVisible(true);
		setSize(width, height);
		setLocation(50, 50);
		window.add(panel1);
		window.add(panel2);
		add(window);
		setResizable(true);
		pack();

	}

}
