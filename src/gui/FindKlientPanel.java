package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import dao.KlientDAO;
import dao.impl.KlientDAOImpl;
import dao.impl.WypozyczenieDAOImpl;
import sql.Egzemplarz;
import sql.Klient;
import sql.Wypozyczenie;

public class FindKlientPanel extends JPanel {

	private JLabel label;
	private JTextField textField;
	private JButton findButton;
	private KlientDAOImpl klientDAOImpl;
	private WypozyczenieDAOImpl wypozyczenieDAOImpl;
	private List<Klient> listOfFindedKlients;
	KlientResultPanel klientResultPanel;

	public FindKlientPanel(EgzemplarzPanel egzemplarzPanel) {

		this.setLayout(new GridBagLayout());

		label = new JLabel("Wpisz imi�, nazwisko lub numer klienta");
		GridBagConstraints c1 = new GridBagConstraints();
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 0;
		c1.gridwidth = 3;
		add(label, c1);

		textField = new JTextField();
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 1;
		c2.gridwidth = 3;
		add(textField, c2);

		findButton = new JButton("Szukaj");
		GridBagConstraints c3 = new GridBagConstraints();
		c3.fill = GridBagConstraints.HORIZONTAL;
		c3.gridx = 0;
		c3.gridy = 2;
		c3.gridwidth = 1;
		add(findButton, c3);
		findButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				klientDAOImpl = new KlientDAOImpl();
				wypozyczenieDAOImpl = new WypozyczenieDAOImpl();

				try {
					listOfFindedKlients = klientDAOImpl.findKlient(textField.getText());
					klientResultPanel = new KlientResultPanel(listOfFindedKlients, new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {

							int indexOfSelectedKlient = klientResultPanel.table.getSelectedRow();
							int idKlient = listOfFindedKlients.get(indexOfSelectedKlient).getIdKlient();
							long rentDate = System.currentTimeMillis();
							Wypozyczenie wypozyczenie = new Wypozyczenie(
									idKlient, egzemplarzPanel.listOfEgzemplarz
											.get(egzemplarzPanel.table.getSelectedRow()).getIdEgzemplarz(),
									rentDate, null, 0, "wypo�yczony");
							try {
								wypozyczenieDAOImpl.insertIntoWypozyczenie(wypozyczenie);
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
							
							LabelPanel rentedPanel = new LabelPanel("Wypo�yczono");
							Window window = new Window("", 500,300, rentedPanel, Window.DISPOSE_ON_CLOSE);
						}
					});
				} catch (SQLException e1) {
					e1.printStackTrace();

				}
				Window window = new Window("Klienci", 3000, 3000, klientResultPanel, Window.DISPOSE_ON_CLOSE);
			}
		});
	}
}
