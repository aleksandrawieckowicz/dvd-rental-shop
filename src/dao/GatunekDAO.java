package dao;

import java.sql.SQLException;
import java.util.List;

import sql.Gatunek;

public interface GatunekDAO {

	public List<Gatunek> getAllGatunek() throws SQLException;
	
	public void insertIntoGatunek(Gatunek gatunek) throws SQLException;
	
	public List<Gatunek> viewAllGatunek() throws SQLException;
}
