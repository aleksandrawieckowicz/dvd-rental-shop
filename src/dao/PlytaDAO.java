package dao;

import java.sql.SQLException;
import java.util.List;

import sql.Plyta;

public interface PlytaDAO {
	
	public void insertIntoPlyta(Plyta plyta ) throws SQLException; 
	
	public List<Plyta> findPlyta (String text) throws SQLException;
	
	public List<Plyta> getAllPlyta() throws SQLException;

}
