package dao;

import java.sql.SQLException;
import java.util.List;

import sql.Rezyser;

public interface RezyserDAO {

	List<Rezyser> getAllRezyser() throws SQLException;
	
	public void insertIntoRezyser(Rezyser rezyser) throws SQLException;
	
	public List<Rezyser> findRezyser (String text) throws SQLException;
}

	
