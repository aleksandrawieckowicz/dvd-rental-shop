package dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.GatunekDAO;
import dao.RezyserDAO;
import dao.mapper.GatunekMapper;
import sql.Gatunek;
import sql.Rezyser;

public class GatunekDAOImpl extends AbstractDAO<Gatunek> implements GatunekDAO{
	
	public List<Gatunek> getAllGatunek() throws SQLException {
		GatunekMapper gatunekMapper = new GatunekMapper();
		return selectQuery("SELECT * FROM GATUNEK", gatunekMapper);
	}
	
	public void insertIntoGatunek(Gatunek gatunek) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("INSERT INTO GATUNEK VALUES (gatunek_seq.nextval,'" + gatunek.getNazwaGatunek() + "') ");
		con.close();
	}
	
	public List<Gatunek> viewAllGatunek() throws SQLException{
		GatunekMapper gatunekMapper = new GatunekMapper();
		return selectQuery("SELECT * FROM GATUNEK", gatunekMapper);	
	}

	
	

	
	
}
