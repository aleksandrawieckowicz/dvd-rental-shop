package dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.management.Query;

import dao.EgzemplarzDAO;
import dao.mapper.EgzemplarzMapper;
import sql.Egzemplarz;
import sql.Plyta;

public class EgzemplarzDAOImpl extends AbstractDAO<Egzemplarz> implements EgzemplarzDAO {

	@Override
	public java.util.List<Egzemplarz> getAllEgzemplarz() throws SQLException {
		EgzemplarzMapper egzemplarzMapper = new EgzemplarzMapper();
		return selectQuery("SELECT * FROM egzemplarz", egzemplarzMapper);
	}

	@Override
	public List<Egzemplarz> showEgzemplarz(int idPlyta) throws SQLException {
		EgzemplarzMapper egzemplarzMapper = new EgzemplarzMapper();
		return selectQuery("SELECT id_egzemplarz, id_plyta FROM egzemplarz WHERE id_plyta = " + idPlyta,
				egzemplarzMapper);
	}

	@Override
	public int countEgzemplarz(String tittle) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(
				"SELECT count(*) FROM egzemplarz e join plyta p ON e.id_plyta = p.id_plyta WHERE p.tytul LIKE '"
						+ tittle + "' GROUP BY e.id_plyta");
		return rs.getInt(1);
	}

	@Override
	public void addEgzemplarz(Plyta plyta) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("INSERT INTO egzemplarz VALUES (egzemplarz_seq.nextval," + plyta.getIdPlyta() + ")");
		con.close();
	}

	@Override
	public Integer isEgzemplarzAvailabe(int idEgzemplarz) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt
				.executeQuery("SELECT id_egzemplarz FROM wypozyczenie WHERE id_egzemplarz LIKE'" + idEgzemplarz + "' AND status = 'wypożyczony'");
		if (rs.next()) {
			return rs.getInt(1);
		} else {
			return null;
		}
	}
}
