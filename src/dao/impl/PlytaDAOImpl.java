package dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.PlytaDAO;
import dao.mapper.PlytaMapper;
import sql.Plyta;

public class PlytaDAOImpl extends AbstractDAO implements PlytaDAO {

	@Override
	public void insertIntoPlyta(Plyta plyta) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
	    stmt.executeQuery("INSERT INTO plyta VALUES (plyta_seq.nextval, '" + plyta.getTytul() + "','" + plyta.getRezyser().getIdRezyser() + "','" 
		+ plyta.getGatunek().getIdGatunek() + "','" + plyta.getCena() + "')");
	    con.close();
	}
// TODO doko�czy� metody wyszukiwania w DAO oraz metoda find ma zwracac imie i nazwisko rezysera jako jeden index w li�cie
	//@Override
	//public List<Plyta> findPlyta(String text) {
	//	PlytaMapper plytaMapper = new PlytaMapper();
	//	return selectQuery("SELECT , mapper);
	//}

	@Override
	public List<Plyta> getAllPlyta() throws SQLException {
		PlytaMapper plytaMapper = new PlytaMapper();
		return selectQuery ("SELECT p.id_plyta, p.tytul, r.imie_rezyser, r.nazwisko_rezyser, g.nazwa,  p.cena  FROM plyta p"+ " "+   
				"JOIN rezyser r ON p.id_rezyser = r.id_rezyser JOIN gatunek g ON p.id_gatunek = g.id_gatunek", plytaMapper);
	}
	@Override
	public List<Plyta> findPlyta(String text) throws SQLException {
		PlytaMapper plytaMapper = new PlytaMapper();
		return selectQuery("SELECT p.id_plyta, p.tytul, r.imie_rezyser, r.nazwisko_rezyser, g.nazwa,  p.cena  FROM plyta p"+ " "+   
				"JOIN rezyser r ON p.id_rezyser = r.id_rezyser JOIN gatunek g ON p.id_gatunek = g.id_gatunek"+ " " + 
				"WHERE LOWER(p.tytul) LIKE'"+ text + "%' OR LOWER(r.imie_rezyser) LIKE '" + text 
				+ "%' OR LOWER(r.nazwisko_rezyser) LIKE '" + text + "%'", plytaMapper);
		
	}
		
}
