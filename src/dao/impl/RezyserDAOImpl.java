package dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.RezyserDAO;
import dao.mapper.RezyserMapper;
import sql.Rezyser;

public class RezyserDAOImpl extends AbstractDAO<Rezyser> implements RezyserDAO {

	@Override
	public List<Rezyser> getAllRezyser() throws SQLException {
		RezyserMapper rezyserMapper = new RezyserMapper();
		return selectQuery("SELECT * FROM REZYSER", rezyserMapper);
	}
	
	@Override
	public void insertIntoRezyser(Rezyser rezyser) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
	    stmt.executeQuery("INSERT INTO rezyser VALUES (rezyser_seq.nextval, '" + rezyser.getImie() + "', '" + rezyser.getNazwisko() + "')");
	    con.close();
		
	}

	@Override
	public List<Rezyser> findRezyser(String text) throws SQLException {
		RezyserMapper rezyserMapper = new RezyserMapper();
		return selectQuery("SELECT * FROM rezyser WHERE imie_rezyser LIKE '" + text + "%' OR nazwisko_rezyser LIKE '" + text +"%' ", rezyserMapper);
		
	}
	
	@Override
	public void deleteFromRezyser(Rezyser rezyser) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("DELETE * FROM rezyser WHERE id_rezyser = " + rezyser.getIdRezyser());
		con.close();
	}
	
	

}
