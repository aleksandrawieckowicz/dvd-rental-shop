package dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.mapper.Mapper;
import sql.Egzemplarz;
import sql.Plyta;
import sql.Rezyser;

public abstract class AbstractDAO<T> {

	static {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	protected List<T> selectQuery(String select, Mapper<T> mapper) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(select);
		List<T> result = mapper.map(rs);
		con.close();
		return result;
	}
	
	protected Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/dvd_rental_shop", "admin", "admin");
	}

	public void deleteFromRezyser(Rezyser rezyser) throws SQLException {
		
	}

	


	
	

}

	
