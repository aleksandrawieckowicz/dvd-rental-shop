package dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.WypozyczenieDAO;
import dao.mapper.WypozyczenieMapper;
import sql.Wypozyczenie;

public class WypozyczenieDAOImpl extends AbstractDAO<Wypozyczenie> implements WypozyczenieDAO {

	@Override
	public List<Wypozyczenie> getWypozyczenie(int idEgzemplarz) throws SQLException {
		WypozyczenieMapper wypozyczenieMapper = new WypozyczenieMapper();
		return selectQuery(
				"SELECT * FROM wypozyczenie " +
		"WHERE id_egzemplarz = " + idEgzemplarz + " AND status = 'wypo�yczony'", wypozyczenieMapper);

	}

	@Override
	public void insertIntoWypozyczenie(Wypozyczenie wypozyczenie) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("INSERT INTO wypozyczenie VALUES (wypozyczenie_seq.nextval," + wypozyczenie.getIdKlient() + ","
				+ wypozyczenie.getIdEgzemplarz() + "," + wypozyczenie.getDataWypozyczenia() + ","
				+ wypozyczenie.getDataZwrotu() + "," + wypozyczenie.getKara() + ",'" + wypozyczenie.getStatus() + "')");
		con.close();
	}

	// Nie wiadomo czy ostatecznie potrzebne wyszukiwanie po kliencie
	@Override
	public List<Wypozyczenie> getWypozyczenieByKlient(int idKlient) throws SQLException {
		WypozyczenieMapper wypozyczenieMapper = new WypozyczenieMapper();
		return selectQuery("SELECT id_wypozyczenie, id_egzemplarz, id_klient, data_wypozyczenia, data_zwrotu, kara "
				+ "FROM wypozyczenie WHERE id_klient = " + idKlient, wypozyczenieMapper);
	}

	@Override
	public void insertDataZwrotu(Wypozyczenie wypozyczenie) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("UPDATE wypozyczenie SET data_zwrotu = " + wypozyczenie.getDataZwrotu() + " WHERE id_wypozyczenie = " + wypozyczenie.getIdWypozyczenie());
		con.close();	
	}
	
	public void changeStatus (Wypozyczenie wypozyczenie) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("UPDATE wypozyczenie SET status = '" + wypozyczenie.getStatus() + "' WHERE id_wypozyczenie = " + wypozyczenie.getIdWypozyczenie());
		con.close();
	}

	@Override
	public void insertKara(Wypozyczenie wypozyczenie) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("UPDATE wypozyczenie SET kara = " + wypozyczenie.getKara() + " WHERE id_wypozyczenie = " 
		+ wypozyczenie.getIdWypozyczenie());
		con.close();
	}
	

	@Override
	public List<Wypozyczenie> getReturnedWypozyczenieByKlient(int idKlient) throws SQLException {
		WypozyczenieMapper wypozyczenieMapper = new WypozyczenieMapper();
		String ex = "SELECT * FROM wypozyczenie WHERE id_klient = " + idKlient + " AND status = 'zwr�cony'";
		return selectQuery(ex , wypozyczenieMapper);
	}

	@Override
	public List<Wypozyczenie> getReturnedWypozyczenieByEgzemplarz(int idEgzemplarz) throws SQLException {
		WypozyczenieMapper wypozyczenieMapper = new WypozyczenieMapper();
		String ex = "SELECT * FROM wypozyczenie WHERE id_egzemplarz = " + idEgzemplarz + " AND status = 'zwr�cony'";
		return selectQuery(ex , wypozyczenieMapper);
	}

	@Override
	public List<Wypozyczenie> getAllReturnedWypozyczenie() throws SQLException {
		WypozyczenieMapper wypozyczenieMapper = new WypozyczenieMapper();
		String ex = "SELECT * FROM wypozyczenie WHERE status = 'zwr�cony'";
		System.out.println(ex);
		return selectQuery(ex , wypozyczenieMapper);
	}
}
