package dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.KlientDAO;
import dao.mapper.KlientMapper;
import sql.Klient;

public class KlientDAOImpl extends AbstractDAO implements KlientDAO {

	public void insertIntoKlient(Klient klient) throws SQLException {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.executeQuery("INSERT INTO klient VALUES (klient_seq.nextval, '" + klient.getImie() + "','"
				+ klient.getNazwisko() + "','" + klient.getMiasto() + "')");
		con.close();
	}

	@Override
	public List<Klient> findKlient(String text) throws SQLException {
		KlientMapper klientMapper = new KlientMapper();
		return selectQuery(
				"SELECT * FROM klient WHERE id_klient LIKE'" + text + "%' OR LOWER(imie) LIKE'" + text
						+ "%' OR LOWER(nazwisko) LIKE'" + text + "%' OR LOWER(miasto) LIKE'" + text + "'",
				klientMapper);
	}

	@Override
	public List<Klient> getAllKlient() throws SQLException {
		KlientMapper klientMapper = new KlientMapper();
		return selectQuery(
				"SELECT * FROM klient",  klientMapper);
	}
	
	
}
