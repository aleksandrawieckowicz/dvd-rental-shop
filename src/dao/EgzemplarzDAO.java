package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sql.Egzemplarz;
import sql.Plyta;

public interface EgzemplarzDAO {

	public List<Egzemplarz> getAllEgzemplarz() throws SQLException;
	
	public List<Egzemplarz> showEgzemplarz(int idPlyta) throws SQLException;
	
	public int countEgzemplarz(String tittle) throws SQLException;
	
	public void addEgzemplarz(Plyta plyta) throws SQLException;
	
	public Integer isEgzemplarzAvailabe (int idEgzemplarz) throws SQLException; 
}
