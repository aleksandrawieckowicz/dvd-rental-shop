package dao.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sql.Wypozyczenie;

public class WypozyczenieMapper implements Mapper<Wypozyczenie> {

	@Override
	public java.util.List<Wypozyczenie> map(ResultSet rs) throws SQLException {
		List<Wypozyczenie> result = new ArrayList();
		while(rs.next()) {
			Wypozyczenie wypozyczenie = new Wypozyczenie();
			wypozyczenie.setIdWypozyczenie(rs.getInt(1));
			wypozyczenie.setIdEgzemplarz(rs.getInt(2));
			wypozyczenie.setIdKlient(rs.getInt(3));
			wypozyczenie.setDataWypozyczenia(rs.getLong(4));
			wypozyczenie.setDataZwrotu(rs.getLong(5));
			wypozyczenie.setKara(rs.getInt(6));
			wypozyczenie.setStatus(rs.getString(7));
			result.add(wypozyczenie);
		}
		return result;
	}
}
