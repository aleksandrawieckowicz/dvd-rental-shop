package dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sql.Klient;

public class KlientMapper implements Mapper {
	
	public List<Klient> map(ResultSet rs) throws SQLException {
		List<Klient> result = new ArrayList();
		while (rs.next()) {
			Klient klient = new Klient();
			klient.setIdKlient(rs.getInt(1));
			klient.setImie(rs.getString(2));
			klient.setNazwisko(rs.getString(3));
			klient.setMiasto(rs.getString(4));
			result.add(klient);
		}
		return result;
		
	}

}
