package dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sql.Gatunek;

public class GatunekMapper implements Mapper<Gatunek>{

	@Override
	public List<Gatunek> map(ResultSet rs) throws SQLException {
		List<Gatunek>  result = new ArrayList();
		while (rs.next()) {
			Gatunek gatunek = new Gatunek();
			gatunek.setIdGatunek(rs.getInt(1));
			gatunek.setNazwaGatunek(rs.getString(2));
			result.add(gatunek);
		}
		
		return result;
	}

	

}
