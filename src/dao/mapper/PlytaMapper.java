package dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sql.Gatunek;
import sql.Plyta;
import sql.Rezyser;

public class PlytaMapper implements Mapper{

	@Override
	public List map(ResultSet rs) throws SQLException {
		List <Plyta> result = new ArrayList();
		while (rs.next()) {
			Plyta plyta = new Plyta();
			plyta.setIdPlyta(rs.getInt(1));
			plyta.setTytul(rs.getString(2));
			plyta.setRezyser(new Rezyser(rs.getString(3), rs.getString(4)));
			plyta.setGatunek(new Gatunek(rs.getString(5)));
			plyta.setCena(rs.getDouble(6));
			result.add(plyta);
		}
		return result;
	}
 
}
