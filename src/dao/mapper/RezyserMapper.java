package dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import sql.Rezyser;

public class RezyserMapper implements Mapper<Rezyser> {

	@Override
	public List<Rezyser> map(ResultSet rs) throws SQLException {
		List<Rezyser> result = new ArrayList<>();
		while(rs.next()) {
			Rezyser rezyser = new Rezyser();
			rezyser.setIdRezyser(rs.getInt(1));
			rezyser.setImie(rs.getString(2));
			rezyser.setNazwisko(rs.getString(3));
			result.add(rezyser);
		}
		return result;
	}
	
}

