package dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface Mapper<T> {
	
	public List<T> map(ResultSet rs) throws SQLException;

}
