package dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sql.Egzemplarz;

public class EgzemplarzMapper implements Mapper<Egzemplarz> {

	@Override
	public List<Egzemplarz> map(ResultSet rs) throws SQLException {
		List<Egzemplarz> result = new ArrayList<>();
		while(rs.next()) {
			Egzemplarz egzemplarz = new Egzemplarz();
			egzemplarz.setIdEgzemplarz(rs.getInt(1));
			egzemplarz.setIdPlyta(rs.getInt(2));
			result.add(egzemplarz);
		}
		return result;

	}
		 
}
