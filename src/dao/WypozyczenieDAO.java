package dao;

import java.sql.SQLException;
import java.util.List;

import sql.Wypozyczenie;

public interface WypozyczenieDAO {
	
	public List<Wypozyczenie> getWypozyczenie (int idEgzemplarz) throws SQLException;

	public void insertIntoWypozyczenie(Wypozyczenie wypozyczenie) throws SQLException;
	
	public List<Wypozyczenie> getWypozyczenieByKlient (int idKlient) throws SQLException;
	
	public void insertDataZwrotu (Wypozyczenie wypozyczenie) throws SQLException;
	
	public void changeStatus (Wypozyczenie wypozyczenie) throws SQLException;
	
	public void insertKara (Wypozyczenie wypozyczenie) throws SQLException;
	
	public List<Wypozyczenie> getReturnedWypozyczenieByKlient(int idKlient) throws SQLException;
	
	public List<Wypozyczenie> getReturnedWypozyczenieByEgzemplarz(int idEgzemplarz) throws SQLException;
	
	public List<Wypozyczenie> getAllReturnedWypozyczenie() throws SQLException;
}
