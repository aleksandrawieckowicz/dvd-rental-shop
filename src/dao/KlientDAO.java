package dao;


import java.sql.SQLException;
import java.util.List;

import sql.Klient;

public interface KlientDAO {

	public void insertIntoKlient (Klient klient) throws SQLException;
	
	public List<Klient> findKlient (String text) throws SQLException;
	
	public List<Klient> getAllKlient() throws SQLException;
}
